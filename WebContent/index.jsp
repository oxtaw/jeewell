<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/view/admin/taglib.jsp"  %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>首页</title>
	<script type="text/javascript" src="${ctxAsset}/common/js/jquery-1.12.4.min.js"></script>
</head>

<body>

	验证码：
	<input type="text" id="captcha" name="captcha" />
	<input type="button" id="validate_captcha_btn" name="validate_captcha_btn" value="验证码校验" />
	<img id="captcha_img" src="${ctx}/captcha/imageCaptcha.jpg" alt="正在加载验证码" onclick="this.src='${ctx}/captcha/imageCaptcha.jpg?'+new Date()" title="单击更换验证码" style="cursor: pointer;" />
		
	<br />
	<br />

	<a href="${ctx}/admin/user/selectUserList" target="_blank">用户列表链接</a>

	<a href="${ctx}/admin/user/gotoUserListPage" target="_blank">用户列表页面</a>
	
	<a href="${ctx}/admin/user/insertUser">新增用户</a>
	
	<a href="${ctx}/admin/user/insertUserList">新增用户列表</a>

</body>

<script type="text/javascript">

	$(function(){
		$("#validate_captcha_btn").click(function(){
			var $ths=$(this);
			var captcha=$("#captcha").val();
			if(captcha.length==0){
				alert("请输入验证码！");
				return;	
			}
			$ths.attr("disabled",true);
			$.post("${ctx}/captcha/validateImageCapctha",{captcha:captcha},function(data){
				alert("data："+data);
				$("#captcha_img").attr("src","${ctx}/captcha/imageCaptcha.jpg?"+new Date());
				$ths.attr("disabled",false);
			});
		});
	});

</script>

</html>