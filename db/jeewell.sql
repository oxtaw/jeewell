/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50549
Source Host           : localhost:3306
Source Database       : jeewell

Target Server Type    : MYSQL
Target Server Version : 50549
File Encoding         : 65001

Date: 2016-07-05 11:15:42
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_admin
-- ----------------------------
DROP TABLE IF EXISTS `t_admin`;
CREATE TABLE `t_admin` (
  `id` varchar(36) NOT NULL COMMENT '主键',
  `username` varchar(32) NOT NULL COMMENT '管理员的用户名称',
  `password` varchar(128) NOT NULL COMMENT '管理员的密码',
  `insertDateTime` datetime DEFAULT NULL COMMENT '新增管理员的日期时间',
  `updateDateTime` datetime DEFAULT NULL COMMENT '修改管理员的日期时间',
  `memo` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `id` varchar(64) NOT NULL,
  `username` varchar(32) NOT NULL COMMENT '用户名称',
  `password` varchar(64) NOT NULL COMMENT '用户密码',
  `insertDateTime` datetime DEFAULT NULL COMMENT '新增的日期时间',
  `updateDateTime` datetime DEFAULT NULL COMMENT '修改的日期时间',
  `memo` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_user_detail
-- ----------------------------
DROP TABLE IF EXISTS `t_user_detail`;
CREATE TABLE `t_user_detail` (
  `id` varchar(64) NOT NULL COMMENT '主键',
  `user_id` varchar(64) NOT NULL COMMENT '用户表(t_user)的id主键',
  `nickname` varchar(32) NOT NULL COMMENT '用户昵称',
  `gender` int(2) NOT NULL DEFAULT '1' COMMENT '性别(1---男,2---女)',
  `birthday` date DEFAULT NULL COMMENT '生日',
  `height` varchar(16) DEFAULT NULL,
  `weight` varchar(16) DEFAULT NULL,
  `insertDateTime` datetime DEFAULT NULL COMMENT '新增的日期时间',
  `updateDateTime` datetime DEFAULT NULL COMMENT '更新的日期时间',
  `memo` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
