package com.test;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.shiro.authc.credential.PasswordService;
import org.junit.Test;

import com.cms.admin.service.AdminService;
import com.cms.kernel.entity.Admin;
import com.cms.kernel.util.SpringUtil;

/**
 * Copyright (C) 2013 2016 Well
 * 文件:AdminTest.java
 * 创建:Well
 * 日期:2016年6月24日 上午8:45:54
 * 来自:
 * 版本:
 * 描述:管理员的测试用例
 */

public class AdminTest
{
	private static Logger logger=LogManager.getLogger(AdminTest.class);
	
	private AdminService adminService=SpringUtil.getBean(AdminService.class);
	
	private PasswordService passwordService=SpringUtil.getBean(PasswordService.class);
	
	/**
	 * 新增管理员的测试用例
	 */
	@Test
	public void testInsertAdmin()
	{
		String username="admin";
		String password=passwordService.encryptPassword("123456");
		
		int result=adminService.insertAdmin(username,password);
		if(result>0)
		{
			logger.info("新增【"+result+"】个管理员成功！");
		}
		else
		{
			logger.error("新增管理员失败！");
		}
	}
	
	/**
	 * 删除管理员的测试用例
	 */
	@Test
	public void testDeleteAdmin()
	{
		String id="8400ea29bb044a3384e89d1312a2cb5c";
		int result=adminService.delete(id);
		if(result>0)
		{
			logger.info("删除【"+result+"】个管理员成功！");
		}
		else
		{
			logger.error("删除管理员失败！");
		}
	}
	
	/**
	 * 修改管理员的测试用例
	 */
	@Test
	public void testUpdateAdmin()
	{
		Admin admin=new Admin();
		admin.setId("cff73c8988074c4caf2c5408aa7ccff0");
		admin.setUpdateDateTime(new Date());
		admin.setPassword(passwordService.encryptPassword("admin"));
		int result=adminService.updateSelective(admin);
		if(result>0)
		{
			logger.info("修改【"+result+"】个管理员成功！");
		}
		else
		{
			logger.error("修改管理员失败！");
		}
	}
	
	/**
	 * 查询管理员列表的测试用例
	 */
	@Test
	public void testSelectAdminList()
	{
		List<Admin> adminList=adminService.selectList();
		if(null==adminList || adminList.size()==0)
		{
			logger.warn("没有查询到用户");
		}
		for(Admin admin:adminList)
		{
			logger.info(admin.getId()+"		"+admin.getUsername()+"		"+admin.getPassword());
		}
	}
	
	/**
	 * 根据非主键的属性值查询单个管理员的测试用例
	 */
	@Test
	public void testSelectAdmin()
	{
		Admin a=new Admin();
		a.setUsername("admin");
		Admin admin=adminService.select(a);
		if(null!=admin)
		{
			logger.info(admin.getId()+"		"+admin.getUsername()+"		"+admin.getPassword());
		}
		else
		{
			logger.warn("没有查询到管理员对象");
		}
	}
	
	/**
	 * 根据主键查询单个管理员的测试用例
	 */
	@Test
	public void testSelectAdminById()
	{
		String id="cff73c8988074c4caf2c5408aa7ccff0";
		Admin admin=adminService.select(id);
		if(null!=admin)
		{
			logger.info(admin.getId()+"		"+admin.getUsername()+"		"+admin.getPassword());
		}
		else
		{
			logger.warn("没有查询到管理员对象");
		}
	}
	
}
