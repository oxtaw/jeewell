package com.test;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import com.cms.kernel.util.IDUtil;

/**
 * Copyright (C) 2013 2016 Well
 * 文件:IDTest.java
 * 创建:Well
 * 日期:2016年5月25日 下午2:06:45
 * 来自:
 * 版本:
 * 描述:主键的测试类
 */

public class IDTest
{
	private Logger logger=LogManager.getLogger(IDTest.class);
	
	@Test
	public void test1()
	{
		String id=IDUtil.generate();
		logger.info(id);
		logger.info(id.length());
	}
	
	@Test
	public void test2()
	{
		String id=IDUtil.generateUUID();
		logger.info(id);
		logger.info(id.length());
	}
}
