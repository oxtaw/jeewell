package com.cms.kernel.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.springframework.format.annotation.DateTimeFormat;

import com.cms.kernel.util.DateTimeUtil;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * Copyright (C) 2013 2016 Well
 * 文件:ABaseEntity.java
 * 创建:Well
 * 日期:2016年5月25日 上午11:25:20
 * 来自:
 * 版本:1.0.0
 * 描述:抽象实体基类，实现该抽象父类的实体子类默认已经拥有下列属性
 * 		主键id
 * 		新增的日期时间
 * 		修改的日期时间
 */

@MappedSuperclass
public abstract class ABaseEntity implements Serializable
{
	@Id
	@GeneratedValue(generator = "UUID")
	private String id; //主键id

	@Column(name = "insertDateTime")
	private Date insertDateTime; // 新增的日期时间，非空，使用默认的转换格式，在springMVC.xml中的37行到41行已经配置完成

	@Column(name = "updateDateTime")
	private Date updateDateTime; // 修改的日期时间，非空，使用默认的转换格式，在springMVC.xml中的37行到41行已经配置完成

	@Column(name = "memo")
	private String memo; // 备注，可空

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public Date getInsertDateTime()
	{
		return insertDateTime;
	}

	public void setInsertDateTime(Date insertDateTime)
	{
		this.insertDateTime = insertDateTime;
	}

	public Date getUpdateDateTime()
	{
		return updateDateTime;
	}

	public void setUpdateDateTime(Date updateDateTime)
	{
		this.updateDateTime = updateDateTime;
	}

	public String getMemo()
	{
		return memo;
	}

	public void setMemo(String memo)
	{
		this.memo = memo;
	}

}
