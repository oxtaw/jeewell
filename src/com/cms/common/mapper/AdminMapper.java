package com.cms.common.mapper;

import com.cms.kernel.entity.Admin;

import tk.mybatis.mapper.common.Mapper;

/**
 * Copyright (C) 2013 2016 Well
 * 文件:AdminMapper.java
 * 创建:Well
 * 日期:2016年6月24日 上午8:48:43
 * 来自:
 * 版本:
 * 描述:管理员的mapper接口
 */

public interface AdminMapper extends Mapper<Admin>
{

}
