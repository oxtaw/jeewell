package com.cms.admin.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cms.admin.service.UserService;
import com.cms.kernel.entity.User;

/**
 * Copyright (C) 2013 2016 Well
 * 文件:UserController.java
 * 创建:Well
 * 日期:2016年5月27日 上午11:21:27
 * 来自:
 * 版本:0.9.0
 * 描述:用户的controller类
 * 		为了区分系统的前台请求和后台请求，所有的后台请求前缀都加上 /admin 
 */

@Controller
@RequestMapping("/admin/user")
public class UserController
{
	@Autowired
	private UserService userService;
	
	/**
	 * 查询用户列表
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/selectUserList",method=RequestMethod.GET)
	@ResponseBody
	public Map<String,Object> selecrUserList(HttpServletRequest request)
	{
		Map<String,Object> responseMap=new HashMap<String,Object>();
		
		List<User> userList=userService.selectUserList();
		responseMap.put("userList", userList);
		
		return responseMap;
	}
	
	/**
	 * 进入用户列表页面
	 * @return 用户列表的模型视图
	 */
	@RequestMapping(value="/gotoUserListPage",method=RequestMethod.GET)
	public ModelAndView gotoUserListPage(HttpServletRequest request,ModelMap modelMap)
	{
		List<User> userList=userService.selectUserList();
		modelMap.put("userList", userList);
		
		return new ModelAndView("userList");
	}
	
	/**
	 * 新增用户
	 * @return
	 */
	@RequestMapping(value="/insertUser",method={RequestMethod.GET,RequestMethod.POST})
	public Map<String,Object> insertUser(HttpServletRequest request)
	{
		Map<String,Object> responseMap=new HashMap<String,Object>();
		
		int result=userService.insertUser("6", "six");
		responseMap.put("result", result);
		
		return responseMap;
	}
	
	/**
	 * 批量新增用户
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/insertUserList",method={RequestMethod.GET,RequestMethod.POST})
	public Map<String,Object> insertUserList(HttpServletRequest request)
	{
		Map<String,Object> responseMap=new HashMap<String,Object>();
		
		int result=userService.insertUserList();
		responseMap.put("result", result);
		
		return responseMap;
	}
	
}
