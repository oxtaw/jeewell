package com.cms.admin.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cms.common.mapper.UserDetailMapper;
import com.cms.common.service.ABaseService;
import com.cms.kernel.entity.UserDetail;

/**
 * Copyright (C) 2013 2016 Well
 * 文件:UserDetailService.java
 * 创建:Well
 * 日期:2016年6月6日 下午3:02:05
 * 来自:
 * 版本:0.9.1
 * 描述:用户明细信息的service类
 */

@Service
public class UserDetailService extends ABaseService<UserDetail>
{
	@Autowired
	private UserDetailMapper userDetailMapper;
}
