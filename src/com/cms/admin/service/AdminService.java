package com.cms.admin.service;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.ui.ModelMap;

import com.cms.common.service.ABaseService;
import com.cms.kernel.entity.Admin;
import com.cms.kernel.exception.JEEWellException;
import com.cms.kernel.util.IDUtil;

/**
 * Copyright (C) 2013 2016 Well
 * 文件:AdminService.java
 * 创建:Well
 * 日期:2016年6月24日 上午8:47:46
 * 来自:
 * 版本:
 * 描述:管理员的service类
 */

@Service
public class AdminService extends ABaseService<Admin>
{
	private static Logger logger=LogManager.getLogger(AdminService.class);
	
	/**
	 * 
	 * @param modelMap
	 * @return
	 */
	public String login(ModelMap modelMap)
	{
		String viewName=null;
		try
		{
			Subject subject=SecurityUtils.getSubject();
			if(null!=subject && subject.isAuthenticated())
			{
                viewName="admin/module/index.jsp";
                modelMap.put("message","用户登录成功！");
			}
			else
			{
                viewName="admin/login.jsp";
                modelMap.put("message","用户登录失败！");
			}
		}
		catch(JEEWellException e)
		{
			logger.error(e);
		}
		return viewName;
	}
	
	/**
	 * 
	 * @param mpdelMap
	 * @return
	 */
	public String logout(ModelMap modelMap)
	{
		String viewName=null;
		try
		{
			Subject subject=SecurityUtils.getSubject();
			if(null!=subject)
			{
				Admin admin=(Admin)subject.getPrincipal();
				subject.logout();
				viewName="admin/login.jsp";
				String message="【"+admin.getUsername()+"】注销成功！";
				modelMap.put("message", message);
				logger.info(message);
			}
		}
		catch(JEEWellException e)
		{
			logger.error(e);
		}
		return viewName;
	}
	
	/**
	 * 当用户上一次选中自动登录时，下一次的登录就调用自动登录
	 * @param admin 管理员对象
	 */
	public void autoLogin(Admin admin)
	{
		String username=admin.getUsername();
		String password=admin.getPassword();
		boolean rememberMe=true;
		UsernamePasswordToken token=new UsernamePasswordToken(username, password,rememberMe);
		SecurityUtils.getSubject().login(token);
	}
	
	/**
	 * 新增管理员
	 * @param username 管理员的用户名称
	 * @param password 管理员的登录密码
	 * @return
	 */
	@Transactional
	public int insertAdmin(String username,String password)
	{
		int result=0;
		try
		{
			Admin admin=new Admin();
			admin.setId(IDUtil.generate());
			admin.setUsername(username);
			admin.setPassword(password);
			admin.setInsertDateTime(new Date());
			admin.setUpdateDateTime(new Date());
			admin.setMemo("测试新增管理员");
			
			result=super.insert(admin);
		}
		catch(Exception e)
		{
			logger.error(e);
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		return result;
	}
	
	/**
	 * 根据管理员的用户名称获取管理员对象
	 * 
	 * @param username 登录名称
	 * @return
	 */
	public Admin getAdminByUsername(String username)
	{
		Admin admin=null;
		
		if(StringUtils.isNotBlank(username))
		{
			Admin a=new Admin();
			a.setUsername(username);
			admin=super.select(a);
		}
		
		return admin;
	}
}
