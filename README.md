jeewell：
	jeewell是开源的JavaEE开发平台，集成流行的开发框架，采用MVC的分层设计思想，
	为JavaEE开发这提供一个安全、高效的开发模式，目前的版本为0.9.4，功能正在逐步完善中。

开发环境： 
	JDK：1.7+ 
	编辑器：Eclipse Mars 
	数据库：Mysql5.5+ 
	服务器：Tomcat7.0+

技术选型： 
	后台： 
		核心框架：Spring Framework 4.0+ 
		视图框架：Spring MVC 4.0+ 
		安全框架：Apache Shiro 1.2
		持久层框架：Mybatis 3.3.0 
				Mybatis mapper 3.2.3 
				Mybatis Pagehelper 4.0.1 
		缓存框架：Ehcache 2.10.0 
		日志框架：log4j2 
		工具框架：Apache Commons等 

		后续的框架还在完善中… 
	前台： 
		暂无

特别鸣谢 
	1. 感谢jeesite开源项目，网址：http://www.jeesite.com/ 
	2. 感谢mybatis工具开源项目，网址：http://www.mybatis.tk/